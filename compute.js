
var requestURL = 'https://iot.martinintw.com/api/v1/data/12345613';
var request = new XMLHttpRequest();
request.open('GET', requestURL);
request.responseType = 'json';
request.send();
request.onload = function() {
  var sensor = request.response;
  make_header();
  datacompute(sensor);
}

function make_header() {
    var myH1 = document.createElement('h1');
    myH1.textContent = "清大鋼琴練習室";
    header.appendChild(myH1);
}

var to_compute = ["Use_Freq", "Most_Freq_time", "Use_Time", "Not_In_Use"];

function datacompute(jsonObj) {
    for(var i = 0; i < to_compute.length; i++) {
        if (to_compute[i] == "Use_Freq") Use_Freq(jsonObj);
        else if (to_compute[i] == "Use_Time") Use_Time(jsonObj);
        else if (to_compute[i] == "Most_Freq_time") Most_Freq_time(jsonObj);
        else if (to_compute[i] == "Not_In_Use") Not_In_Use(jsonObj);
        
    }

}


function Use_Freq(jsonObj) {
    var myArticle = document.createElement('article');
    var myH2 = document.createElement('h2');
    var dates = [];
    var dates_count = {};
    var time_distance;
    
    for( var j = 0 ; j < jsonObj.length; j++) {
        
        var date = jsonObj[j]["updated_at"].slice(0, 10);
        if(j > 0) time_distance = (Number(jsonObj[j]["updated_at"].slice(11, 13))*60 +  Number(jsonObj[j]["updated_at"].slice(14, 16))) - (Number(jsonObj[j-1]["updated_at"].slice(11, 13))*60 +  Number(jsonObj[j-1]["updated_at"].slice(14, 16)));
        
        if ( dates.indexOf(date) == -1) { //date not in dates
            dates.push(date);
            dates_count[date] = 1;
        }
        else {
            if(time_distance >= 10) {
                dates_count[date]++;
            }
            
        }
    }
    myH2.textContent = "使用次數";
    myArticle.appendChild(myH2);
    for(var k = 0; k < dates.length; k++) {
        var myPara1 = document.createElement('p');
        myPara1.textContent = dates[k] + ' : ' + dates_count[dates[k]] + '次';
        myArticle.appendChild(myPara1);
    }
    section.appendChild(myArticle);

    displayLineChart(dates,dates_count);

}
 

function Use_Time(jsonObj) {
    var myArticle = document.createElement('article');
    var myH2 = document.createElement('h2');
    var dates = [];
	var dates_2 = [];
    var dates_count = {};
	var dates_count_2 = {};
	var start = 0;
	var end = 0;
	var idle;
	var vib_o;
	var time_distance;
    
    for(var j = 0; j < jsonObj.length; j++)
    {
        var date = jsonObj[j]["updated_at"].slice(0, 10);
        if(j > 0)
        {
            idle = (Number(jsonObj[j]["updated_at"].slice(11, 13))*60 + Number(jsonObj[j]["updated_at"].slice(14, 16))) - (Number(jsonObj[j-1]["updated_at"].slice(11, 13))*60 + Number(jsonObj[j-1]["updated_at"].slice(14, 16)));

			if(j != jsonObj.length - 1)
			{
				vib_o = (Number(jsonObj[j + 1]["updated_at"].slice(11, 13))*60 + Number(jsonObj[j + 1]["updated_at"].slice(14, 16))) - (Number(jsonObj[j]["updated_at"].slice(11, 13))*60 + Number(jsonObj[j]["updated_at"].slice(14, 16)));
			}
			else
			{
                if(idle >= 10) dates_count[date] += 1; //for the last data
			}
        }
        if ( dates.indexOf(date) == -1) { //date not in dates
            dates.push(date);
            dates_count[date] = 1;
        }
		else
		{
			if(idle >= 10)
			{
				if(vib_o >= 10)
				{
                    if(start==0) end = j-1;
                    else start =j;
                    dates_count[date] += 1;// one min for those vibrate once
                    end = j-1;
				}
				else
				{
                    var START = (Number(jsonObj[start]["updated_at"].slice(11, 13))*60 + Number(jsonObj[start]["updated_at"].slice(14, 16)));
                    end = j-1;
                    var END = (Number(jsonObj[end]["updated_at"].slice(11, 13))*60 + Number(jsonObj[end]["updated_at"].slice(14, 16)));

                    if(END - START == 0)
                    {
                        dates_count[date] += 1;
                    }
					    
					else{
                        dates_count[date] += (END - START);
                    }
						
                    start = j; 
				}
            }
            if(idle<0) start = j;
		}
    }
	for( var j = 0 ; j < jsonObj.length; j++) {
        
        var date_2 = jsonObj[j]["updated_at"].slice(0, 10);
        if(j > 0) time_distance = (Number(jsonObj[j]["updated_at"].slice(11, 13))*60 +  Number(jsonObj[j]["updated_at"].slice(14, 16))) - (Number(jsonObj[j-1]["updated_at"].slice(11, 13))*60 +  Number(jsonObj[j-1]["updated_at"].slice(14, 16)));
        
        if ( dates_2.indexOf(date_2) == -1) { //date not in dates
            dates_2.push(date_2);
            dates_count_2[date_2] = 1;
        }
        else {
            if(time_distance >= 10) {
                dates_count_2[date_2]++;
            }
            
        }
    }
    myH2.textContent = "平均使用時間";
    myArticle.appendChild(myH2);
    for(var k = 0; k < dates.length; k++)
	{
        var myPara1 = document.createElement('p');
        myPara1.textContent = dates[k] + ' : ' + (toDecimal2(dates_count[dates[k]] / dates_count_2[dates_2[k]])) + " min(s)";
	    myArticle.appendChild(myPara1);
    }
	section.appendChild(myArticle); 
}


function toDecimal2(x) { 
    var f = parseFloat(x); 
    if (isNaN(f)) { 
      return false; 
    } 
    var f = Math.round(x*100)/100; 
    var s = f.toString(); 
    var rs = s.indexOf('.'); 
    if (rs < 0) { 
      rs = s.length; 
      s += '.'; 
    } 
    while (s.length <= rs + 2) { 
      s += '0'; 
    } 
    return s; 
}


function Most_Freq_time(jsonObj) {
    var myArticle = document.createElement('article');
    var myH2 = document.createElement('h2');
    var hours = [];
    var hours_count = {};
    var time_distance;
    var new_hour;
    
    for(var i = 0 ; i < 24; i += 2) {
        hours.push(i);
        hours_count[i] = 0;
    }

    for( var j = 0 ; j < jsonObj.length; j++) {
        var hour = Number( jsonObj[j]["updated_at"].slice(11, 13) );
        if(j > 0) {
            time_distance = 
            (Number( jsonObj[j]["updated_at"].slice(11, 13) )*60 +Number( jsonObj[j]["updated_at"].slice(14, 16) ) ) - 
            (Number( jsonObj[j-1]["updated_at"].slice(11, 13) )*60 +  Number( jsonObj[j-1]["updated_at"].slice(14, 16) ) );

            new_hour = (( jsonObj[j]["updated_at"].slice(11, 13) ) != ( jsonObj[j-1]["updated_at"].slice(11, 13) ))?true:false;

        }

        if((j == 0) || (new_hour == true) || (time_distance >= 10)) {
            if(hour%2 == 0) hours_count[hour]++;
            else hours_count[hour - 1]++;
        }
            
    }
    myH2.textContent = "最常使用時段";
    myArticle.appendChild(myH2);
    for(var k = 0; k < hours.length; k ++) {
        var myPara1 = document.createElement('p');
        myPara1.classList.add('most_fre');
        if(hours[k] == 0) myPara1.textContent = 12 + ' A.M. : ' + hours_count[hours[k]] + '次';
        else if(hours[k] <= 12) myPara1.textContent = hours[k] + ' A.M. : ' + hours_count[hours[k]] + '次';
        else if(hours[k] > 12)  myPara1.textContent = (hours[k] - 12) + ' P.M. : ' + hours_count[hours[k]] + '次';
        myArticle.appendChild(myPara1);
    }
    section.appendChild(myArticle);

    displayLineChart2(hours, hours_count);
}


function Not_In_Use(jsonObj) {
    var myArticle = document.createElement('article');
    var myH2 = document.createElement('h2'); 
    myH2.textContent = "閒置鋼琴";
    myArticle.appendChild(myH2);
    create_animation_block(myArticle);

    var myDate = new Date();  
    var IDLE = false;
 
    //compute time
    var time_count = Number(jsonObj[jsonObj.length-1]["updated_at"].slice(11, 13))*60 +  Number(jsonObj[jsonObj.length-1]["updated_at"].slice(14, 16));
    var now_time = myDate.getHours()*60 + myDate.getMinutes();

    //compute date
    var date_count = Number(jsonObj[jsonObj.length-1]["updated_at"].slice(8, 10));
    var month_count = Number(jsonObj[jsonObj.length-1]["updated_at"].slice(5, 7));
    var year_count = Number(jsonObj[jsonObj.length-1]["updated_at"].slice(0, 4));
    var day_value = date_count + month_count*30 + year_count;
    var now_day = (myDate.getMonth() +1) * 30 + myDate.getDate() + myDate.getFullYear();

  
    if(  Math.abs(now_time - time_count) <=10 && now_day == day_value) IDLE = false;
    else IDLE = true; 
 
 
    var day_difference = now_day - day_value;
    if(day_difference<0) day_difference = 1440 - day_difference;

    
    
    var myPara1 = document.createElement('p');
    if(IDLE){
        myPara1.textContent = 'IDLE for' + ' '+ day_difference  + ' ' + 'Days' + ' ' + (now_time - time_count)  + ' ' + 'Minutes' ;
        Animation(myPara1);
    } 
    else {
        myPara1.textContent = 'ACTIVE'; 
        Animation(myPara1);
    } 

}
