function create_animation_block(myArticle) {
    var myH2 = document.createElement('h3');
    var mySTop = document.createElement('span');
    var myS1 = document.createElement('span');
    var myS2 = document.createElement('span');
    var myS3 = document.createElement('span');

    myH2.classList.add('ml1');
    mySTop.classList.add('text-wrapper');
    myS1.classList.add('line');
    myS1.classList.add('line1');
    myS2.id = 'timer';
    myS2.classList.add('letters');
    myS2.innerHTML = 'TIMER';
    myS3.classList.add('line');
    myS3.classList.add('line2');

    mySTop.appendChild(myS1);
    mySTop.appendChild(myS2);
    mySTop.appendChild(myS3);
    myH2.appendChild(mySTop);
    myArticle.appendChild(myH2);
    section.appendChild(myArticle);

}

function create_block() {
    var myArticle = document.createElement('article');
    var myCanvas = document.createElement('canvas');


    myArticle.classList.add('animated');
    myArticle.classList.add('bounceInDown');
    myCanvas.id = 'lineChart';
    myCanvas.height = "220";
    myCanvas.width =  "450";


    myArticle.appendChild(myCanvas);
    section.appendChild(myArticle);

}

function create_block2() {
    var myArticle = document.createElement('article');
    var myCanvas = document.createElement('canvas');


    myArticle.classList.add('animated');
    myArticle.classList.add('bounceInDown');
    myCanvas.id = 'lineChart2';
    myCanvas.height = "220";
    myCanvas.width =  "450";


    myArticle.appendChild(myCanvas);
    section.appendChild(myArticle);

}
 

function displayLineChart(dates,dates_count) {
    create_block();
    var labels = [];
    var data = [];


    for(var k = 0; k < dates.length; k++) {
        labels[k] = dates[k];       
    }

    for(var k = 0; k < dates.length; k++) {
        data[k] = dates_count[dates[k]];
    }

    var data = {

        labels, 
        datasets: [
            {
                label: "My Second dataset",
                fillColor: "rgba(34,157,171,0.2)",
                strokeColor: "rgba(34,157,171,1)",
                pointColor: "rgba(34,157,171,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(34,157,171,1)",
                data
            }
        ]
    };

    var ctx = document.getElementById('lineChart').getContext("2d");
    var options = { };
    var lineChart = new Chart(ctx).Line(data, options);
}

function displayLineChart2(dates,dates_count) {
    create_block2();
    var labels = [];
    var data = [];


    for(var k = 0; k < dates.length; k++) {
        labels[k] = dates[k];       
    }

    for(var k = 0; k < dates.length; k++) {
        data[k] = dates_count[dates[k]];
    }

    var data = {

        labels, 
        datasets: [
            {
                label: "My Second dataset",
                fillColor: "rgba(34,157,171,0.2)",
                strokeColor: "rgba(34,157,171,1)",
                pointColor: "rgba(34,157,171,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(34,157,171,1)",
                data
            }
        ]
    };

    var ctx = document.getElementById('lineChart2').getContext("2d");
    var options = { };
    var lineChart = new Chart(ctx).Line(data, options);
}


function Animation(text) {
    document.getElementById('timer').innerHTML = text.textContent;

    // Wrap every letter in a span
    $('.ml1 .letters').each(function(){ 
        $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
    });

    anime.timeline({loop: true})
        .add({
        targets: '.ml1 .letter',
        scale: [0.3,1],
        opacity: [0,1],
        translateZ: 0,
        easing: "easeOutExpo",
        duration: 600,
        delay: function(el, i) {
            return 70 * (i+1)
        }
        }).add({
        targets: '.ml1 .line',
        scaleX: [0,1],
        opacity: [0.5,1],
        easing: "easeOutExpo",
        duration: 700,
        offset: '-=875',
        delay: function(el, i, l) {
            return 80 * (l - i);
        }
        }).add({
        targets: '.ml1',
        opacity: 0,
        duration: 1000,
        easing: "easeOutExpo",
        delay: 1000
        });  
}


    //https://codepen.io/bernardo/pen/ugApF
    //https://ithelp.ithome.com.tw/articles/10195394
    //http://tobiasahlin.com/blog/chartjs-charts-to-get-you-started/